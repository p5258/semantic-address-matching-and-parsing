# SEMANTIC ADDRESS MATCHING AND PARSING

<!-- INSEE launched the GAIA project in search of new innovative ways to automatically manipulate addresses. We showcase our contributions to this project while endovering in exploring different methodologies aiming at resolving two of its main purposes : the parsing and matching of addresses. Deep Learning approches were applied in order to accomplish the parsing process. Models such as CRF and BILSTM were successfully implemented and displayed near perfect results when assessing their performances while others more mathematical methods such as the Hidden Markov Model Viterbi were less successful. A semantic index was created in order to fulfill the matching process allowing to extract a match to a given requested address and few others that bear resemblance to it. Finally, a web application was implemented demonstrating the solid results of both the semantic index and the Deep Learning approach methods. -->

## BRÈVE DESCRIPTION

Le Projet GAIA de l'INSEE vise à rechercher de nouvelles méthodes innovantes pour le traitement et la manipulation automatique des adresses postales. Le présent répertoire Git comporte notre contribution à ce projet.Nous avons exploré ici différentes méthodologies visant à résoudre deux de ses principaux objectifs : l'analyse syntaxique et la mise en correspondance des adresses. 

Des approches d'apprentissage profond ont été appliquées afin d'accomplir le processus d'analyse syntaxique. Des modèles tels que CRF et BILSTM ont été mis en œuvre avec succès et ont affiché des résultats presque parfaits lors de l'évaluation de leurs performances, tandis que d'autres méthodes plus mathématiques telles que le modèle de Markov caché de Viterbi ont eu moins de succès. Les scripts et fichiers concernant cette partie du projet sont consultables dans `./parsing/`.

Pour l'algorithme de matching permettant de réaliser une correspondance entre une adresse demandée donnée et quelques autres qui lui ressemblent, nous avons créer un index sémantique le modèle *distiluse-base-multilingual-cased-v1* et la librairie *FAISS* de **Meta AI**. Une application web a alors été mise en place pour démontrer les bons résultats de l'index sémantique et de l'approche d'apprentissage profond d'extraction informations. Les scripts et fichiers concernant cette partie du projet sont consultables dans `./matching/`.

## EXECUTION DE L'API

Cet api a été développé pour rendre opérationnel les modèles construits dans le cadre du projet intitulé "Semantic Adress Matching and Parsing". Pour exécuter l'api, il faut :

- Cloner le dépôt git en local : `git clone https://gitlab.com/p5258/semantic-address-matching-and-parsing.git`

- Se placer dans le dossier : `cd semantic-address-matching-and-parsing/`

- Installer les librairies requises : `pip install -r requirements.txt`

- Enfin taper : `python3 matching/application.py`.

La page d'accueil s'ouvre et donne la possibilité d'exécuter soit le parsing soit le matching.

### MATCHING   
En cliquant sur le bouton matching, on obtient une fenêtre avec un formulaire à deux champs :
- Le premier spécifie le nombre d'adresse que l'on souhaite retourner dans le tableau de résulat;
- Le second permet de renseigner l'adresse à chercher.
Une fois les deux champs renseingés, on lance la recherche et on obtient le résultat sous la forme d'un tableau. 

L'index pour la recherche est contenu dans le dossier matching/ressources/model et son nom débute par `index_topo_ign_`. Il s'agit d'un fichier `.gz`.

Comme nous avons construit deux grands index, la recherche d'une adresse dans l'un d'eux revient tout simplement à remplacer l'index par défaut *(qui a été construit sur une petit échantillon de 2000 adresses pour la démo)* par cet index et à modifier l'argument de la fonction `load_index()` dans le fichier `application.py`. Par ailleurs, il faudra remplacer le fichier `topo_ign_sample.csv` par le fichier *csv* `topo_ign_<<numerodufichier>>.csv` (fichier déjà traité) utilisé pour la construction de cet index. Ce fichier se trouve dans le dossier `final_data` du datalab. 


### PARSING
Pour accéder à cette page, il suffit de revenir sur la page d'accueil et de cliquer sur le bouton parsing. Puis on renseigne
une adresse à parser et on clique sur le bouton prédire. 
NB : Le modèle de parsing est contenu dans le dossier `matching/ressources/model`.


Pour rappel, tous les index sont contenus dans le dossier data/index du service "matching" du datalab de l'insee et les csv prétraités dans le dossier `data/final_data_topo`. Nous ne pouvons pas les mettre dans un repo git car ils sont de très grandes tailles. 
