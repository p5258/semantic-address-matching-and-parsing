## Importation des librairies
import numpy as np
import pandas as pd
from flask import Flask, request, jsonify, render_template, url_for, redirect
from matching import *
from parsing import *
import pickle
import gzip
import string
import re
import faiss
from sentence_transformers import SentenceTransformer 
from tensorflow.keras.models import load_model
import fasttext


CONFIG_PATH = "ressources/"

############################ MATCHING 
##_____________ load
def load_index(name_index):
    with gzip.open(CONFIG_PATH + name_index+ '.gz', "rb") as file_index:
        index = pickle.load(file_index)
    return index


def load_data(name_data):
    data = pd.read_csv(CONFIG_PATH + name_data+'.csv', header=0, low_memory=False)
    return data


def load_model_st(model_path="ressources/model"):
    return SentenceTransformer(model_path)


# _______ Normalisation d'une chaine
def string_norm(chaine):
    pattern = r"[" + string.punctuation + "]"
    result = re.sub(pattern, " ", chaine.lower())
    return result


# ________ Recherche dans un unique index
def search_from_one_index(query, model, index_=None, topo_data=None, top=5):
    result = {}
    query = [string_norm(query[0])]

    find = Search(query, model)

    find.search_with_L2(index_, num_results=top)

    result = find.save_top_result(topo_data, col_display="data")

    return result


############################ APP BUILDING
def create_faiss_app():

    app = Flask(__name__)

    ## outils pour le matching
    index = load_index("index_topo_ign_sample")
    data = load_data("topo_ign_sample")
    model_matching = load_model_st()

    ## outils pour le parsing
    model_parsing = load_model("ressources/bilstm_ner_100.h5")
    fastt = fasttext.load_model("ressources/cc.fr.32.bin")

    ## Page d'accueil
    @app.route("/", methods=['GET', 'POST'])
    def home():
        if request.method == 'POST':
            if request.form['submit_button']=='matching':
                return redirect(url_for('matching'))
            else:
                return redirect(url_for('parsing'))
        return render_template("home.html")
    
    ## Page pour le matching
    @app.route("/matching", methods=["POST", "GET"])
    def matching():

        if request.method == "POST":
            top = request.form["top"]
            query = [request.form["adresse"]]
        else:
            top = request.args.get("top")
            query = [request.args.get("adresse")]

        if top is None:
            top = 10
            query=['14 avenue robert schuman']

        result = search_from_one_index(
            query, model_matching, index_=index, topo_data=data, top=int(top)
        )

        return render_template("matching.html", result=result)

    ## Page pour le parsing
    @app.route("/parsing", methods=["POST", "GET"])
    def parsing():

        if request.method == "POST":
            query = request.form["adresse"]
        else:
            query = request.args.get("adresse")

        if query is None:
            query='Appt A15, 17 D Square des Cloteaux, 35200 Rennes' # adresse par défaut

        # Prédiction
        add = Parsing(query)
        result = add.prediction(model_parsing, fastt)

        return render_template("parsing.html", result=result)

    return app


if __name__ == "__main__":
    application = create_faiss_app()
    application.run(host='127.0.0.1',port=8000,debug=False)
