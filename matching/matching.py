import numpy as np


class Search:
    def __init__(self, query, model):
        """
        # param :
        query (`list` : `str`) : liste contenant uniquement l'adresse à chercher
        model ( obj : `Transformer`) : modèle préentrainé

        args :
        - distance (:obj:`numpy.array` de `float32`) : métrique (L2 ou Cosine)
            entre la requête et les éléments de l'index retenu

        - id (:obj:`list` de `int`) : identifiant des adresses les plus proches
            de l'adresse contenu dans la requête

        # return :
        - None
        """
        self.model = model
        self.query = query
        self.distance = None
        self.id = None

    def search_with_L2(self, index_L2, num_results=10):
        """Réalise la recherche d'une adresse en utilisant comme métrique la norme L2
        # param :
        index_L2 (:obj: `faiss`) : index des adresses
        data     (:obj: `dataframe pandas`) : dataframe ayant subit le processing
            de la classe Featuring

        num_results (`int` ) : top des addresses les plus proches de la requêtes

        # return :
        None
        """

        ## Embedding de la requête
        vector = self.model.encode(self.query)

        ## Recupération des distance et identifiant des k addresses le plus proches
        distance, id = index_L2.search(
            np.array(vector).astype("float32"), k=num_results
        )

        ## Mise à jour de distance et id
        self.distance = distance[0]
        self.id = id[0]

    def save_top_result(self, data, col_display):
        """Renvoie les résultats de la recherche dans un dictionnaire
        # param :
            - data (:obj: `dataframe pandas`) : dataframe ayant subit le processing
                de la classe Featuring

            - col_display (`str`) : Colonne du dataframe à afficher
            - metric (`str`) : Métrique utilisée

        # return :
            - metric (`Dict`) : dictionnaire dont la clé est est l'adresse et la valeur
                                est une liste contenant la valeur de la métrique et
                                l'identifiant dans la base topo
        """

        result = {}
        if self.id is not None:
            for i, idx_ in enumerate(self.id):
                result[data[data.id == idx_][col_display].values[0]] = [
                    round(float(self.distance[i]), 4),
                    data[data.id == idx_]["cleabs"].values[0],
                ]
        return result
