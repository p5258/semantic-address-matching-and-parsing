import re
import numpy as np
class Parsing():

    def __init__(self, address):
        self.address = address

    def cleaning(self):
        self.address = self.address.upper()
        self.address = " ".join([i for i in self.address.split(" ") if i != ""])
        self.address = re.sub(r"[^-A-ZàâäéèêëïîôöùûüÿçÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸæÆŒœa-z0-9\' ]+", "", self.address)
        self.address = self.address.split(" ")

    def prediction(self, model, ft, maxlen=145, dim=32):

        result = dict()

        tags = ['CODPOST', 'NUMVOIE', 'SUFF', 'LIBCOM', 'CPLADR', 'LIBVOIE', 'PAD']

        result["ADDRESS"] = self.address

        for i in ['NUMVOIE', 'SUFF', 'LIBVOIE', 'CPLADR', 'CODPOST', 'LIBCOM', 'PAD']:
            result[i] = ""
        
        self.cleaning()
        n = len(self.address)

        test = np.zeros((1, maxlen, dim))
        
        for i, token in enumerate(self.address):
            
            test[0][i] = list(ft.get_word_vector(token))
        
        p = model.predict(test)
        p = np.argmax(p, axis=-1)

        label = [tags[p[0][i]] for i in range(n)]
           
        for i, token in enumerate(self.address):

            key = label[i]

            if result[key] == "":

                result[key] += token
            else :
                result[key] += f" {token}"

        final = dict()
        tags = ['CODPOST', 'NUMVOIE', 'SUFF', 'LIBCOM', 'CPLADR', 'LIBVOIE', 'PAD']
        final['Code postal'] = result['CODPOST']
        final['Numéro de la voie'] = result['NUMVOIE']
        final['Libellé de la voie'] = result['LIBVOIE']
        final['Suffixe'] = result['SUFF']
        final['Libellé de la commune'] = result['LIBCOM']
        final["Complément d'adresse"] = result['CPLADR']

        for key, value in final.items():
            if value=="":
                final[key]='---'
            else:
                final[key] = value.lower()
        
        final = dict((k, final[k]) for k in final.keys() if final[k] !="---")
        return final