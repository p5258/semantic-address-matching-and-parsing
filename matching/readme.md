Cet api a été développé pour rendre opérationnel les modèles construits dans le 
cadre du projet intitulé "SEMANTIC Adress Matching and Parsing".


## EXECUTION DE L'API 
Pour exécuter l'api, il faut tout d'abord cloner le repo git en local. puis, à l'aide de 
l'invite de commandes, se placer dans le dossier correspondant et taper "python3 application.py".

La page d'accueil s'ouvre et donne la possibilité d'exécuter soit le parsing soit le matching.

## MATCHING   
En cliquant sur le bouton matching, on obtient une fenêtre avec un formulaire à deux champs :
- Le premier spécifie le nombre d'adresse que l'on souhaite retourner dans le tableau de résulat;
- Le second permet de renseigner l'adresse à chercher.
Une fois les deux champs renseingés, on lance la recherche et on obtient le résultat sous la 
forme d'un tableau. 

L'index pour la recherche est contenu dans le dossier matching/ressources/model et son nom 
débute par index_topo_ign_. Il s'agit d'un fichier .gz 
Comme nous avons construit deux grands index, la recherche d'une adresse dans l'un d'eux revient 
tout simplement à remplacer l'index par défaut (qui a été construit sur une petit échantillon de 2000 adresses pour la démo)
par cet index et à modifier l'argument de la fonction load_index() dans le fichier application.py. Par ailleurs, il faudra
remplacer le fichier topo_ign_sample.csv par le fichier csv topo_ign_numerodufichier.csv (fichier déjà traité) utilisé pour la construction de cet index. Ce fichier se trouve dans le dossier final_data du datalab. 


## PARSING
Pour accéder à cette page, il suffit de revenir sur la page d'accueil et de cliquer sur le bouton parsing. Puis on renseigne
une adresse à parser et on clique sur le bouton prédire. 
NB : Le modèle de parsing est contenu dans le dossier `Matching/ressources/model`.


Pour rappel tous les index sont contenus dans le dossier data/index du service "matching" du datalab de l'insee et les csv prétraités dans le dossier data/final_data_topo. Nous ne pouvons pas les mettre dans un repo git car ils sont de très grandes tailles. 