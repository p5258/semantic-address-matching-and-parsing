###################################################
# Ce fichier récapitule tout le code utilisé pour
# construire un index en utilisant la BD TOPO.
###################################################

## 0. CHEMEIN DU REPERTOIRE
CONFIG_PATH = 'data/'
CONFIG_PATH_INDEX ='data/index/'
CONFIG_PATH_TOPO = 'data/final_data_topo/'

## 1. CHARGEMENT DES LIBRAIRIES
!pip install sentence_transformers faiss_gpu brotli

###################################################
## 2. IMPORTATION DES LIBRAIRIES
import pandas as pd
import numpy as np
import pickle
from sentence_transformers import SentenceTransformer 
import numpy as np
import faiss
import time 
import torch
import gzip
import string
import re

###################################################
## 3. CREATION DES CLASSES DE TRAITEMENT DES DATA

#--------------------------------------------------
## 3.a. Classes de prétraitement des données
#--------------------------------------------------
###################################################

#_______ Fonction globale de normalisation d'une chaine
def string_norm(chaine):
    pattern = r'[' + string.punctuation + ']'
    result = re.sub(pattern, ' ', chaine.lower())
    return result

#________________________________________________
class LoadFeaturing():
    
    def __init__(self, file_name, data=None):
        self.file_name = file_name
        self.df_topo= pd.read_csv(CONFIG_PATH+file_name, header=0, low_memory=False) if data is None else data
            

    def preprocessing(self, data_commune):
        """ Réalise un preprocessing 
            rapide de la base de données

            # return :
            - None
        """

        # identifiant numérique des adresses (index du dataframe)
        self.df_topo = self.df_topo.rename({'Unnamed: 0':'id'}, axis=1)

        self.df_topo['insee_commune'] = self.df_topo['insee_commune'].astype('string')

        # Ajout des communes dans le dataframe
        self.df_topo = pd.merge(self.df_topo, data_commune, how='inner', left_on='insee_commune', right_on='COM')

        # Supprimer les ligneS dont toutes les cellules on NAN
        self.df_topo.dropna(axis=0, how='all', inplace=True)
        

        # 2. Fonction pour créer les adresses dans la bd topo
        def build_adress(row):

            adresse = []

            # Complément d'adresse
            if not str(row['designation_de_l_entree'])=='nan':
                adresse.append(row['designation_de_l_entree'])

            # Numéro de la rue
            if not str(row['numero'])=='nan':
                adresse.append(str(row['numero']))

            # Suffixe
            if not str(row['indice_de_repetition'])=='nan':
                adresse.append(str(row['indice_de_repetition']))

            # Nom de la rue
            if not str(row['nom_1'])=='nan':
                adresse.append(row['nom_1'])

            # Code postal
            if not str(row['code_postal'])=='nan':
                adresse.append(str(row['code_postal']))

            # Nom de la commune
            if not str(row['NCC'])=='nan':
                adresse.append(str(row['NCC']))

            ad = ' '.join(adresse)

            return string_norm(ad)

        # 3. appliquer la fonction build_adress sur chaque ligne de df_topo
        self.df_topo['data'] = self.df_topo.apply(build_adress, axis=1)

        self.df_topo = self.df_topo.loc[:,['data', 'cleabs', 'id']]

    def save_data(self):
        self.df_topo.to_csv(CONFIG_PATH+self.file_name+'.csv')
        
#--------------------------------------------------
## 3.b. Classe d'encodage des données
#--------------------------------------------------
###################################################

class Transformer():
    
    def __init__(self, model_path):
        """
        # param : 
        model_path (`str`) : chemin du transformer à charger

        # args : 
        - model ( obj : `sentenceTransformer`) : modèle préentrainé
        - embeddings (:obj:`numpy.array` de `float32`) : vecteur d'embeddings 
                                                            des adresses
        - index (:obj: `faiss`) : index construit à partir du vecteur d'em-
                                beddings
        """
        self.model = SentenceTransformer(model_path)

        ## Vérifier l'existence d'un GPU sur la machine
        if torch.cuda.is_available():
            self.model = self.model.to(torch.device("cuda"))

        self.embeddings = None
        self.index = None


    def show_model_device(self):
        print(self.model.device)



    def encode_data(self, data, column):
        
        """ Réalise l'embedding des addresses contenu dans un dataframe
            en utilisant le modèle transformers

        # param :
        - data (obj: dataframe `pandas`) : base des addresses et de leurs annotations
        - column (`str`) : nom de la colonne sur laquelle l'encodage doit être appliqué

        # return :
        - None
        """
     
        self.embeddings =  self.model.encode(data[column].to_list(), show_progress_bar=True)


    def build_index_Cosine(self, data):
        """ Construit l'index pour lequel une recherche exhaustive sera fait en calculant le 
        produit scalaire entre la query et les élémements de l'index

        # param : 
        data (:obj: dataframe `pandas`) : base contenant pour chaque adresse son identifiant

        # return :
        - None
         """

        ## Changement du type des vecteurs d'embeddings 
        emb = np.array([np.round(embedding,5) for embedding in self.embeddings]).astype("float32")

        ## Initialisation de l'index avec la dimension d'embedding
        index = faiss.IndexFlatIP(emb.shape[1])

        ## Mapping de l'index pour ajouter les identifiant avec la méthode add_with_ids()
        index = faiss.IndexIDMap(index)

        ## Normalisation des vecteur d'embeddings
        faiss.normalize_L2(emb)

        ## Ajout des vecteur d'embedding de chaque adresse et de l'identifiant associé
        index.add_with_ids(emb, data['id'].values)

        ## Mise à jour de l'index
        self.index = index


    def build_cluser_index_Cosine(self, data, nlist=1000, nprobe=50):
        """ Construit l'index comme la fonction précédent, mais en plus regroupe les vecteurs dans 
            des cellules de veroni

            # param : 
            data (:obj: dataframe `pandas`) : base contenant pour chaque adresse son identifiant
            nlist(:int: ) : nombre de cluster à former
            nprobe (:init: ) : nombre de cluster à parcourir lors de la recherche

            # return :
            - None
        """

        ## Changement du type des vecteurs d'embeddings
        emb = np.array([np.round(embedding,4) for embedding in self.embeddings]).astype("float32")
        
        ## Utiliser pour assigner un vecteur à un cluster
        quantizer = faiss.IndexFlatL2(emb.shape[1])

        ## Initialisation de l'index partitionnées
        index = faiss.IndexIVFFlat(quantizer, emb.shape[1], nlist)

        index.nprobe = nprobe

        ## Normalisation des vecteur d'embeddings
        faiss.normalize_L2(emb)

        ## Entrainement et ajout des vecteurs dans l'index
        index.train(emb)
        # index.add(embeddings)

        ## Mapping de l'index pour ajouter les identifiant avec la méthode add_with_ids()
        index = faiss.IndexIDMap(index)

        ## Ajout des vecteur d'embedding de chaque adresse et de l'identifiant associé
        index.add_with_ids(emb, data['id'].values)

        ## Mise à jour de l'index
        self.index = index


    def build_cluster_PCA_index(self, data, nlist, pca_dim, nprobe=5):

        """ Construit l'index comme la fonction précédent, mais en plus regroupe les vecteurs dans 
            des cellules de veroni et exécute une ACP en amont pour réduire la dimension des vecteurs
            d'embeddings

            # param : 
            data (:obj: dataframe `pandas`) : base contenant pour chaque adresse son identifiant
            nlist(:int: ) : nombre de cluster à former
            nprobe (:int: ) : nombre de cluster à parcourir lors de la recherche
            pca_dim (:int:) : dimension du vecteur d'embedding après exécution de l'ACP

            # return :
            - None
        """
        ## Changement du type des vecteurs d'embeddings
        emb = np.array([np.round(embedding,5) for embedding in self.embeddings]).astype("float32")

        ## Initialisation de l'index avec la dimension d'adresse
        quantizer = faiss.IndexFlatL2(pca_dim)
        index = faiss.IndexIVFFlat(quantizer, pca_dim, nlist)

        index.nprobe = nprobe

        ## Réduction de dimension
        pca_matrix = faiss.PCAMatrix(emb.shape[1], pca_dim, 0, False)

        index = faiss.IndexPreTransform(pca_matrix, index)

        # co = faiss.GpuMultipleClonerOptions()
        # co.useFloat16 = True
        # index = faiss.index_cpu_to_all_gpus(index, co=co)
        
        res = faiss.StandardGpuResources()
        index = faiss.index_cpu_to_gpu(res, 0, index)
        
        ## Entrainement
        index.train(emb)

        ## Ajout des vecteur d'embedding de chaque adresse et de l'identifiant associé
        index.add_with_ids(emb, data['id'].values)

        ## Mise à jour de l'index
        self.index = faiss.index_gpu_to_cpu(index)  


    def save_index(self, name_index):
        """ Compresse l'index en .gz
            # param : 
            - name_index (`str`) : nom de l'index

            # return :
            - None
        """

        with gzip.open(CONFIG_PATH_INDEX+name_index+'.gz', "wb") as out:
            pickle.dump(self.index, out)
        
    def load_index(name_index):
        """ Charger un index sérialisé
            # param : 
            - name_index (`str`) : nom de l'index
        """
        with gzip.open(CONFIG_PATH_INDEX +name_index+'.gz', "rb") as file_index:
            index = pickle.load(file_index)
        return index


#--------------------------------------------------
## 3.c. Classe de requêtage de l'index
#--------------------------------------------------
###################################################

class Search():

    def __init__(self, query, model):
        """
            # param : 
            query (`list` : `str`) : liste contenant uniquement l'adresse à chercher
            model ( obj : `Transformer`) : modèle préentrainé

            args : 
            - distance (:obj:`numpy.array` de `float32`) : métrique (L2 ou Cosine)
                entre la requête et les éléments de l'index retenu
                                    
            - id (:obj:`list` de `int`) : identifiant des adresses les plus proches 
                de l'adresse contenu dans la requête
            
            # return :
            - None
        """
        self.model = model
        self.query = query
        self.distance = None
        self.id = None

    def search_with_L2(self, index_L2, num_results=10):
        """ Réalise la recherche d'une adresse en utilisant comme métrique la norme L2
            # param :
            index_L2 (:obj: `faiss`) : index des adresses
            data     (:obj: `dataframe pandas`) : dataframe ayant subit le processing
                de la classe Featuring

            num_results (`int` ) : top des addresses les plus proches de la requêtes

            # return :
            None
        """

        ## Embedding de la requête
        vector = self.model.encode(self.query)

        ## Recupération des distance et identifiant des k addresses le plus proches
        distance, id = index_L2.search(np.array(vector).astype("float32"), k=num_results)  

        ## Mise à jour de distance et id              
        self.distance = distance[0]
        self.id = id[0]


    def search_with_Cosine(self, index_Cosine, num_results=10):
        """ Réalise la recherche d'une adresse en utilisant comme métrique le produit scalaire
            # param :
            index_Cosine (:obj: `faiss`) : index des adresses
            data     (:obj: `dataframe pandas`) : dataframe ayant subit le processing
                de la classe Featuring

            num_results (`int` ) : top des addresses les plus proches de la requêtes

            # return :
            None
        """

        ## Embedding de la requête
        vector = self.model.encode(self.query)

        ## Normalisation du vecteur d'embedding de la requete (<u, v> = cos(u, v) si L2(u)=L2(v)=1)
        faiss.normalize_L2(vector)

        ## Recupération des distance et identifiant des k addresses le plus proches
        distance, id = index_Cosine.search(np.array(vector).astype("float32"), k=num_results) 

        ## Mise à jour de distance et id               
        self.distance = distance[0]
        self.id = id[0]


    def display_address(self, data, col_display, metric):
        """ Affichage des addresses
            # param :
            - data (:obj: `dataframe pandas`) : dataframe ayant subit le processing
                de la classe Featuring

            - col_display (`str`) : Colonne du dataframe à afficher
            - metric (`str`) : Métrique utilisée

            # return : 
            None

        """
        if self.id is not None:
            for i, idx in enumerate(self.id):
                print('_________________________________________________________________')
                print("\n")
                print(f"Addresse {i+1} : {data[data.id==idx][col_display].values[0]} ")
                print("\n")
                print(f"Identifiant : {data[data.id==idx]['cleabs'].values[0]}")
                print("\n")
                print(f"{metric} : {round(float(self.distance[i]),4)}")


    def save_top_result(self,data, col_display):
        """ Renvoie les résultats de la recherche dans un dictionnaire
        # param :
            - data (:obj: `dataframe pandas`) : dataframe ayant subit le processing
                de la classe Featuring

            - col_display (`str`) : Colonne du dataframe à afficher
            - metric (`str`) : Métrique utilisée

        # return : 
            - metric (`Dict`) : dictionnaire dont la clé est est l'adresse et la valeur
                                est une liste contenant la valeur de la métrique et 
                                l'identifiant dans la base topo 
        """

        result = {}
        if self.id is not None:
            for i, idx_ in enumerate(self.id):
                result[data[data.id==idx_][col_display].values[0]] = [round(float(self.distance[i]),4), data[data.id==idx_]['cleabs'].values[0]]
        return result 
    
    
###################################################
## 4. CREATION DES FONCTIONS UTILES

#-----------------------------------------------------
## 4.a. Construction de l'index à partir de la bd topo
#-----------------------------------------------------
###################################################

def build_index_from_file(data_commune, file_name=None, data=None, nlist=2000, nprobe=50, pca_dim=100, type_index="cluster_pca", target='data', model_name='sentence-transformers/distiluse-base-multilingual-cased-v1'):
    
    # Load & Preprocessing
    df_process = LoadFeaturing(file_name)

    df_process.preprocessing(data_commune)
    
    # Save data
    df_process.df_topo.to_csv(CONFIG_PATH_TOPO+file_name+'.csv')
    
    # build and save index
    model = Transformer(model_name)

    model.encode_data(df_process.df_topo, column=target)

    if type_index=="cluster_pca":
        model.build_cluster_PCA_index(df_process.df_topo, nlist=nlist, pca_dim=pca_dim, nprobe=nprobe)    
    elif type_index=="cluster_cosine":
        model.build_cluser_index_Cosine(df_process.df_topo, nlist=nlist, nprobe=nprobe)
    elif type_index=="cosine":
        model.build_index_Cosine(df_process.df_topo)

    model.save_index('index_'+file_name)

    return model.index

#--------------------------------------------------
## 4.b. Recherche dans un unique index
#--------------------------------------------------
###################################################
def search_from_one_index(file_name, query, index_=None, topo_data=None,top=5,metric='cosine', model_name='sentence-transformers/distiluse-base-multilingual-cased-v1'):
    
    result = {}
    query = [string_norm(query[0])] 

    model = Transformer(model_name)

     # On charge le fichier de données obtenu après prétraitement de la bd topo
    if topo_data is None:
        topo_data = pd.read_csv(CONFIG_PATH_TOPO+file_name+'.csv', header=0)
        
    # On charge l'index si index_ est None
    if index_ is None:
        index_ = Transformer.load_index('index_'+file_name) 

    find = Search(query, model.model)

    find.search_with_L2(index_, num_results=top)

    result = find.save_top_result(topo_data,col_display='data')

    for i, (key, value) in enumerate(result.items()):

        if i<top:
            print('_________________________________________________________________')
            print("\n")
            print(f"Addresse {i+1} : {key}")
            print("\n")
            print(f"{metric} : {value[0]} ")
            print("\n")
            print(f"cleabs : {value[1]}")

    return result



#--------------------------------------------------
## 4.b. Recherche d'une adresse dans un index
#--------------------------------------------------
###################################################
def search_from_all_index(*list_file, query,top=5,metric='L2', model_name='sentence-transformers/distiluse-base-multilingual-cased-v1'):

    result = {}   
    
    # Normalisation de la requête
    query = [string_norm(query[0])]

    # Encodage de la requête
    model = Transformer(model_name)

    for file_name in list_file:

        # Importation du fichier de données obtenu après prétraitement de la bd topo
        topo_data = pd.read_csv(CONFIG_PATH_TOPO+file_name+'.csv', header=0)

        # Importation de l'index
        index = Transformer.load_index('index_'+file_name)

        # Switcher l'index du CPU à tout les GPU disponible
        if torch.cuda.is_available():
            co = faiss.GpuMultipleClonerOptions()
            co.useFloat16 = True
            index = faiss.index_cpu_to_all_gpus(index, co=co)

        # Recherche 
        find = Search(query, model.model)

        find.search_with_L2(index, num_results=top)

        result.update(find.save_top_result(topo_data,col_display='data'))

    #False for L2 norm
    final = sorted(result.items(), key = lambda x : x[1][0], reverse=False)   

    for i, (key, value) in enumerate(result.items()):
    
        if i<top:
            print('_________________________________________________________________')
            print("\n")
            print(f"Addresse {i+1} : {key}")
            print("\n")
            print(f"{metric} : {value[0]} ")
            print("\n")
            print(f"cleabs : {value[1]}")

    return result


###################################################
## 4. APPLICATION

#--------------------------------------------------
## 4.c. Application
#--------------------------------------------------
###################################################

## - Chargement du modèle
model = 'sentence-transformers/distiluse-base-multilingual-cased-v1' 

## - Chargement du fichier contenant le nom des commune et les code insee correspondant
data_commune = pd.read_csv(CONFIG_PATH+'commune2021_filter.csv', header=0)

## - Construction de l'index
file_name = ['topo_ign_00','topo_ign_01','topo_ign_02','topo_ign_03','topo_ign_04', 'topo_ign_05']

start = time.time()
for name_ in file_name:
    idx = build_index_from_file(data_commune, file_name=name_, data=None, nlist=2000, nprobe=50, pca_dim=100, type_index="cluster_pca", target='data', model_name='sentence-transformers/distiluse-base-multilingual-cased-v1')
print((time.time()-start)/60)



#--------------------------------------------------
## 4.d. Requêtage d'un seul index
#--------------------------------------------------
###################################################

start = time.time()
index_name = 'topo_ign_05'
a = search_from_one_index(file_name=index_name ,topo_data=None, index_=None, query=['6415 rte monnetier-mornex des 3 lacs 74560 '],top=5, model_name=model)
print('\n')
print(time.time()-start)


#--------------------------------------------------
## 4.d. Requêtage de plusieurs index
#--------------------------------------------------
###################################################

start = time.time()
a=search_from_all_index('topo_ign_05',  query=['2 imp des fleurs 69700 saint-romain-en-gier @!-'], top=10, metric='L2',model_name=model)
print(time.time()-start)