from utilities import *
from keras.callbacks import EarlyStopping
from keras.utils import to_categorical
import pickle

if __name__ == "__main__":
    
    print("\n", "<===================== Loading data =====================>", "\n")
    data = load_sample_as_df(k = 3000)
    data.head()
    print("\n", "<===================== DONE =====================>", "\n")
    print("\n", "<===================== Preparing data =====================>", "\n")
    data = data_preparation(data)
    print("\n", "<===================== DONE =====================>", "\n")

    print("\n", "<===================== Getting X and Y =====================>", "\n")
    x = np.array(data["data"].tolist())
    y = np.array(data["label"].tolist())
    y = to_categorical(y, num_classes=n_tags)
    X_train, X_test, y_train, y_test = data_split(x, y)
    print("\n", "<===================== DONE =====================>", "\n")

    
    print("\n", "<===================== Loading model =====================>", "\n")
    model1 = bilstm_ner()
    model2 = crf_ner()
    model3 = bilstm_crf_ner()
    early = EarlyStopping( monitor='val_loss', patience = 2)

    print("\n", "<===================== Training model started 1=====================>", "\n")
    history1 = model1.fit(X_train, y_train,  epochs = 100, workers = 16, validation_data=(X_test, y_test), batch_size = 250, callbacks=[early])
    print("\n", "<===================== Training ended =====================>", "\n")

    print("\n", "<===================== Converting model1 =====================>", "\n")
    model1.save('./output/bilstm_ner_100.h5')
    
    
    print("\n", "<===================== Saving history plot 1=====================>", "\n")
    plot_history(history1, no = "bilstm", show = False)
    

    print("\n", "<===================== Training model started 2=====================>", "\n")
    history2 = model2.fit(X_train, y_train,  epochs = 100, workers = 16, validation_data=(X_test, y_test), batch_size = 250, callbacks=[early])
    print("\n", "<===================== Training ended =====================>", "\n")

    print("\n", "<===================== Converting model2 =====================>", "\n")
    model2.save('./output/crf_ner.h5')
    
    
    print("\n", "<===================== Saving history plot 2=====================>", "\n")
    plot_history(history2, no = "crf", show = False)


    print("\n", "<===================== Training model started 3=====================>", "\n")
    history3 = model3.fit(X_train, y_train,  epochs = 100, workers = 16, validation_data=(X_test, y_test), batch_size = 250, callbacks=[early])
    print("\n", "<===================== Training ended =====================>", "\n")

    print("\n", "<===================== Converting model 3=====================>", "\n")
    model3.save('./output/bilstm_crf_ner.h5')
    
    
    print("\n", "<===================== Saving history plot 1=====================>", "\n")
    plot_history(history3, no = "bilstm_crf", show = False)

    #print("\n", "<===================== Pickling sets =====================>", "\n")
    #output = open('./output/X_train.saved', 'wb')
    #p = pickle.Pickler(output)              
    #p.dump(X_train)                              
    #output.close()

    #output = open('./output/y_train.saved', 'wb')
    #p = pickle.Pickler(output)              
    #p.dump(y_train)                              
    #output.close()

    #output = open('./output/X_test.saved', 'wb')
    #p = pickle.Pickler(output)              
    #p.dump(X_test)                              
    #output.close()

    #output = open('./output/y_test.saved', 'wb')
    #p = pickle.Pickler(output)              
    #p.dump(y_test)                              
    #output.close()

    print("\n", "<===================== Starting model evaluation =====================>", "\n")
    test_pred1 = model1.predict(X_test, verbose=1, batch_size=500)
    test_pred2 = model2.predict(X_test, verbose=1, batch_size=500)
    test_pred3 = model3.predict(X_test, verbose=1, batch_size=500)

    print(evaluation(y_test, test_pred1))
    print(evaluation(y_test, test_pred2))
    print(evaluation(y_test, test_pred3))
   
    
    print("\n", "<===================== Task finished =====================>", "\n")
