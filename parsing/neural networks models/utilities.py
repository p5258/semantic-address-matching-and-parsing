import json
import random
from multiprocessing import Pool
from functools import reduce
from keras.utils import to_categorical
import pandas as pd
import numpy as np
from transformers import CamembertModel, CamembertTokenizer
from keras.models import Model, Sequential
from keras.layers import LSTM, Embedding, Dense, TimeDistributed, Dropout, Bidirectional, Input
from keras_contrib.layers import CRF
from keras_contrib.losses import crf_loss
from keras_contrib.metrics import crf_accuracy
import torch
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
import fasttext

### some variables

tags = ['CODPOST', 'NUMVOIE', 'SUFF', 'LIBCOM', 'CPLADR', 'LIBVOIE', 'PAD']
n_tags = len(tags)
maxlen = 145
dim = 32

ft = fasttext.load_model('./fasttext_fr_model/cc.fr.32.bin')
#tokenizer = CamembertTokenizer.from_pretrained("camembert-base")
#camembert = CamembertModel.from_pretrained("camembert-base")
#camembert.eval() # disable dropout (or leave in train mode to finetune)


# loading all data function

def load_data():
    
    root = './Json_Files/'

    with open(root + "fichierA.json", "r") as f:
        dataA = json.load(f)
    
    with open(root + "fichierB.json", "r") as f:
        dataB = json.load(f)
    
    with open(root + "fichierC.json", "r") as f:
        dataC = json.load(f)
    
    data = list(dataA.values()) + list(dataB.values()) + list(dataC.values())

    return data

# load sample data function

def load_sample(k):

    all_data = load_data()
    
    random.seed(27)
    n = len(all_data)
    
    sample = random.choices(list(range(n)), k = k)
    sample_data = [all_data[i] for i in sample]

    return sample_data


def load_data_as_df():
    
    root = './Json_Files/'

    dataA = pd.read_json(root + "fichierA.json", orient='index')
    dataB = pd.read_json(root + "fichierB.json", orient='index')
    dataC = pd.read_json(root + "fichierC.json", orient='index')
    
    data = pd.concat([dataA, dataB, dataC])

    n = len(data)
    data = data.set_index(pd.Index(range(n)))

    data["data"] = data["data"].apply(lambda x : x.split(' '))

    return data


def load_sample_as_df(k):
    
    root = './Json_Files/'

    dataA = pd.read_json(root + "fichierA.json", orient='index')
    dataB = pd.read_json(root + "fichierB.json", orient='index')
    dataC = pd.read_json(root + "fichierC.json", orient='index')
    
    data = pd.concat([dataA, dataB, dataC])
    data = data.sample(n = k, random_state = 1)
    n = len(data)
    data = data.set_index(pd.Index(range(n)))

    data["data"] = data["data"].apply(lambda x : x.split(' '))

    return data


# preparing data function

def prepare_address(dict_):

    """
    This function will be used for address annotated mapping
    """

    address = [ dict_['data'].split(' ') ]
    annotation = []
    
    for tup in dict_['label']:
        annotation += [tup[2]]*(tup[1]-tup[0])

    annotation = [annotation]

    return (address, annotation)


def fusion(tup1, tup2):

    """
    This function will be used for reduce step
    """

    return (tup1[0] + tup2[0], tup1[1] + tup2[1])


def data_prepared(data):

    with Pool() as pool:
        mapping = pool.map(prepare_address, data)
    
    return reduce(fusion, mapping)

def get_annot(label):
    
    annotation = []
    for elt in label:
        
        annotation += [elt[2]]*(elt[1]-elt[0])
  
    annotation =  [ tags.index(j) for j in annotation] + [n_tags - 1]*(maxlen - len(annotation))
  
    return annotation

def get_embeddings(add):

  #x = np.zeros((maxlen, 1024))
  x = np.zeros((maxlen, dim))

  #encoded_address = tokenizer.encode(add)
  #encoded_address = torch.tensor(encoded_address).unsqueeze(0)
  #x[:len(add)] = camembert(encoded_address)[0].detach().numpy()[0][1:-1]

  for i, token in enumerate(add):
      
      x[i] = list(ft.get_word_vector(token))

  return x

def data_preparation(data):

    y = np.array(data["label"].apply(get_annot).to_list())
    y = to_categorical(y, num_classes=n_tags)

    print('-----------Labels preparation done-------------')

    x = np.array(data["data"].apply(get_embeddings).to_list())
        
    return train_test_split(x, y, test_size=0.2)

# data preprocessing for training

def tags2ixd_pad(y_data, maxlen):
    
    """
    This function converts labels in index and applies a padding to  the output vector. At the end, It returns a one-hot version.
    y_data : list of list which contains address annotations
    maxlen: length of annotations padded

    """
    k = len(y_data)
    y = np.zeros((k, maxlen), dtype = np.int32)

    for i, ents in enumerate(y_data):
        y[i] =  [ tags.index(j) for j in ents] + [n_tags - 1]*(maxlen - len(ents))

    return to_categorical(y, num_classes=n_tags)


def token2ixd_pad(x_data, maxlen):
    
    k = len(x_data)
    x = np.zeros((k, maxlen, 768 ))

    #for i, add in enumerate(x_data):

        #encoded_address = tokenizer.encode(add)
        #encoded_address = torch.tensor(encoded_address).unsqueeze(0)
        #x[i][:len(add)] = camembert(encoded_address)[0].detach().numpy()[0][1:-1]
  
    return x


def data_split(x, y):

    return train_test_split(x, y, test_size=0.2)


## constructing model

def bilstm_ner():

    model = Sequential()
    model.add(Bidirectional(LSTM(units=100, return_sequences=True, recurrent_dropout=0.1)))
    model.add(TimeDistributed(Dense(n_tags, activation="softmax")))
    model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])
    model.build((None, maxlen, dim))

    return model

def crf_ner():

    input = Input(shape=(maxlen, dim))
    model2 = TimeDistributed(Dense(150, activation="tanh"))(input)
    crf = CRF(n_tags, learn_mode='marginal') 
    #crf = CRF(n_tags) # CRF layer
    out = crf(model2)  # output
    model2 = Model(input, out)
    #model2.compile(optimizer="adam", loss=crf_loss, metrics=[crf_accuracy, "accuracy"])
    model2.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])

    return model2

def bilstm_crf_ner():

    input = Input(shape=(maxlen, dim))
    model1 = Bidirectional(LSTM(units=100, return_sequences=True, recurrent_dropout=0.1))(input)
    model1 = TimeDistributed(Dense(50, activation="tanh"))(model1)
    crf = CRF(n_tags, learn_mode='marginal')
    #crf = CRF(n_tags)
    out = crf(model1)  # output

    model1 = Model(input, out)
    #model1.compile(optimizer="adam", loss=crf_loss, metrics=[crf_accuracy, "accuracy"])
    model1.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])

    return model1

def plot_history(history, no, show = False):
  
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(1, len(acc) + 1)

    fig, ax = plt.subplots(1, 2, constrained_layout=True, figsize=(30, 12), dpi=80)

    ax[0].plot(epochs, acc, label = "Training Accuracy", color='darkblue')
    ax[0].plot(epochs, val_acc, label = "Validation Accuracy", color='darkgreen')
    ax[0].grid(alpha=0.3)
    ax[0].title.set_text('Training Vs Validation Accuracy')
    ax[0].fill_between(epochs, acc, val_acc, color='crimson', alpha=0.3)
    plt.setp(ax[0], xlabel='Epochs')
    plt.setp(ax[0], ylabel='Accuracy')


    ax[1].plot(epochs, loss, label = "Training Loss", color='darkblue')
    ax[1].plot(epochs, val_loss, label = "Validation Loss", color='darkgreen')
    ax[1].grid(alpha=0.3)
    ax[1].title.set_text('Training Vs Validation Loss')
    ax[1].fill_between(epochs,loss, val_loss, color='crimson', alpha=0.3)
    plt.legend(['train', 'val'], loc='upper left')
    plt.setp(ax[1], xlabel='Epochs')
    plt.setp(ax[1], ylabel='Loss')

    if show :
        plt.show()
    else:
        plt.savefig(f"./output/history{no}.png")


def evaluation(gt, pred):

    true = np.argmax(gt, axis=-1)
    hat = np.argmax(pred, axis=-1)

    true = [ [tags[i] for i in elt if i != 6] for elt in true]
    hat = [[tags[k] for k in hat[i][:len(value)]] for i, value in enumerate(true)]
    true = [i for elt in true for i in elt]
    hat = [i for elt in hat for i in elt]

    return classification_report(true, hat)


def one_address_prediction(address, model, maxlen):

    test = np.zeros((1, maxlen, 768 ))
    address = address.split(" ")
    n = len(address)
    #token = tokenizer.encode(address)
    #token = torch.tensor(token).unsqueeze(0)
    #test[0][:n] = camembert(token)[0].detach().numpy()[0][1:-1]
    p = model.predict(test)
    p = np.argmax(p, axis=-1)

    label = [tags[p[0][i]] for i in range(n)]

    result = {
        "token": address, "label": label
    }

    return result
