
from viterbi_solution_testing import *
from tqdm import trange


#set the random seed
random.seed(1234)

#Splitting into training and test sets
#train_set, test_set = train_test_split(load_data(),train_size=0.8)

train_set, test_set = train_test_split(load_sample_as_df(100000),train_size=0.9)

#Get length of training and test sets
print("size of train data", len(train_set))
print("size of testdata", len(test_set))



  
# Getting list of tagged words in training set
train_tagged_words = [tup for sent in train_set for tup in sent]

# Running on entire test dataset would take more than 3-4hrs. 
# Let's test our Viterbi algorithm on a few sample sentences of test dataset


# list of tagged words in test set
test_run_base = [tup for sent in test_set for tup in sent]

# list of  words which are untagged in test set
test_tagged_words = [[tup[0] for tup in sent] for sent in test_set]
test_tags = [[tup[1] for tup in sent] for sent in test_set]

print(" total length of tagged words in training corpus" , len(train_tagged_words))
print(" number of untagged sequences of words in testing corpus", len(test_tagged_words))


"""K=30000"""

import random
#k=len(test_tagged_words)
k=10000
random.seed(27)
sample_data_test=[]
sample_test_run_base=[]

sample = random.choices(list(range(len(test_tagged_words))), k = k)
for i in sample : 
  sample_data_test.append(test_tagged_words[i])
  sample_test_run_base.append(test_tags[i])


# tokens
tokens = [pair[0] for pair in train_tagged_words]
# vocabulary
V = set(tokens)
# number of pos tags in the training corpus
T = set([pair[1] for pair in train_tagged_words])

for tag in T: 
  print ("nb de " , tag, "dans la base test" ,total_nb_tag(tag,sample_test_run_base))
# Create numpy array of no of pos tags by total vocabulary
t = len(T)


v = len(V)
print("number of words in train data" , v)

w_given_t = np.zeros((t, v))




# creating t x t (pos tags x pos tags)transition matrix of tags
# each column is t2, each row is t1
# thus M(i, j) represents P(tj given ti)
tags_matrix = np.zeros((len(T), len(T)), dtype='float32')
for i, t1 in enumerate(list(T)):
    for j, t2 in enumerate(list(T)): 
        tags_matrix[i, j] = t2_given_t1(t2, t1,train_tagged_words)[0]/t2_given_t1(t2, t1,train_tagged_words)[1]
# convert the matrix to a df for better readability
tags_df = pd.DataFrame(tags_matrix, columns = list(T), index=list(T))


plot_transition_matrix(tags_df)
plot_frequent_transition_matrix(tags_df)






#example in DataBase
#print(Viterbi(['7', 'CHEMIN', 'DE', 'SAINT', 'ROMAN', '30470', 'AIMARGUES'],train_tagged_words))

#example prediction not in DataBase
print(Viterbi(['13', 'CONTOUR','ANTOINE' , 'DE', 'SAINT', 'EXUPERY', '35170', 'BRUZ'],tags_df,train_tagged_words))



# tagging the test sentences

tagged_seq =[]
for i in trange(len(sample_data_test)):
  tagged_seq.append(Viterbi(sample_data_test[i],tags_df, train_tagged_words))

tagged_seq_results=[[tagged_seq[j][i][1] for i in range(len(tagged_seq[j]))] for j in range(len(tagged_seq))]

# Get accuracy of model
check = [i for i, j in zip(tagged_seq_results, sample_test_run_base) if i == j] 
accuracy = len(check)/len(tagged_seq_results)
print("accuracy ", accuracy)

#confusion matrix

wrong_tags(tagged_seq_results, sample_test_run_base,T)
number_each_tag(sample_test_run_base,T,tagged_seq_results, sample_test_run_base)



real_words=[]
pred_words=[]
for i in sample_test_run_base:
    for j in i :
      real_words.append(j)
  
for i in tagged_seq_results:
    for j in i :
      pred_words.append(j)
report_classification(real_words, pred_words,T)
