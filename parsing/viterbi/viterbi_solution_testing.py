#Importing libraries
import nltk, re, pprint
import numpy as np
import pandas as pd
import requests
import matplotlib.pyplot as plt
import seaborn as sns
import pprint, time
import random
from sklearn.model_selection import train_test_split
from nltk.tokenize import word_tokenize
import seaborn as sns
import codecs

import dataframe_image as dfi
from sklearn.metrics import classification_report

import json

def load_data():
  with open("C:\\Users\\ASUS\\Desktop\\pfe\\viterbi\\fichierA.json", "r") as f:
    dataA = json.load(f)
  with open( "C:\\Users\\ASUS\\Desktop\\pfe\\viterbi\\fichierB.json", "r") as f:
    dataB = json.load(f)
  with open( "C:\\Users\\ASUS\\Desktop\\pfe\\viterbi\\fichierC.json", "r") as f:
    dataC = json.load(f)
  data = list(dataA.values()) +  list(dataB.values()) + list(dataC.values())
  for i in data :
    i['data']=i['data'].split(' ')
  nl_data= [[(i['data'][j],i['tags'][j]) for j in range(len(i['tags']))] for i in data]
  return nl_data


def load_sample_as_df(k):
  
    all_data = load_data()
    random.seed(27)
    n = len(all_data)
    sample = random.choices(list(range(n)), k = k)
    sample_data = [all_data[i] for i in sample]

    return sample_data






# compute word given tag: Emission Probability
def word_given_tag(word, tag, train_bag ):
    tag_list = [pair for pair in train_bag if pair[1]==tag]
    count_tag = len(tag_list)
    w_given_tag_list = [pair[0] for pair in tag_list if pair[0]==word]
    count_w_given_tag = len(w_given_tag_list)
    
    return (count_w_given_tag, count_tag)

# compute tag given tag: tag2(t2) given tag1 (t1), i.e. Transition Probability

def t2_given_t1(t2, t1, train_bag ):
    tags = [pair[1] for pair in train_bag]
    count_t1 = len([t for t in tags if t==t1])
    count_t2_t1 = 0
    for index in range(len(tags)-1):
        if tags[index]==t1 and tags[index+1] == t2:
            count_t2_t1 += 1
    return (count_t2_t1, count_t1)



def plot_transition_matrix(df):
      # heatmap of tags matrix
      # T(i, j) indicates P(tag j given tag i
      with sns.axes_style("white"):
        f, ax = plt.subplots(figsize=(12, 10))
        ax= sns.heatmap(df, annot=True,linewidths=.2, square=True,cbar_kws={"orientation": "horizontal"})
        plt.title('Transition Matrix of all Tags', fontdict=None, loc='center', pad=None)
        plt.savefig(f"./output/tansition_matrix.png")


def plot_frequent_transition_matrix(df):
      # frequent tags
      # filter the df to get P(t2, t1) > 0.5
      # heatmap of tags matrix
      # T(i, j) indicates P(tag j given tag i)
      tags_frequent = df[df>0.5]
      with sns.axes_style("white"):
        f, ax = plt.subplots(figsize=(12, 10))
        ax= sns.heatmap(tags_frequent, annot=True,linewidths=.2, square=True,cbar_kws={"orientation": "horizontal"})
       
        plt.title('Transition Matrix of Frequent Tags', fontdict=None, loc='center', pad=None)
        plt.savefig(f"./output/frequent_tansition_matrix.png")

# Viterbi Heuristic

def Viterbi(words, table_df, train_bag):
    state = []
    
    #tags_df=transition_matrix(T,train_bag)
    for key, word in enumerate(words):
        T=['LIBCOM','CODPOST','NUMVOIE','CPLADR','LIBVOIE','SUFF']
        #initialise list of probability column for a given observation
        p = [] 
        for tag in T:
            if key == 0:
                transition_p = table_df.loc['LIBCOM', tag]
            else:
                transition_p = table_df.loc[state[-1], tag]*pmax
                
            # compute emission and state probabilities
            emission_p = word_given_tag(words[key], tag,train_bag)[0]/word_given_tag(words[key], tag,train_bag)[1]
            state_probability = emission_p * transition_p    
            p.append(state_probability)
            
        pmax = max(p)
        # getting state for which probability is maximum
        state_max = T[p.index(pmax)] 
        state.append(state_max)
    return list(zip(words, state))



def total_nb_tag(tag,data_test):
      count=0
      for i in data_test:
        for p in i: 
          if p==tag:
                count+=1;
      return count 
        

       

def nb_wrong_tags(t1,t2,wrongp):
  count=0
  for i in wrongp:
    for j in range(len(i[0])):
        if i[0][j] ==t2 and i[1][j] ==t1:
          count+=1
  return count


def wrong_tags(predicted_data, real_data,T): 
  wrongpair=[[i,j] for i, j in zip(predicted_data, real_data) if i != j]
  wrong_matrix = np.zeros((len(T), len(T)), dtype='float32')
  for i, t1 in enumerate(list(T)):
      for j, t2 in enumerate(list(T)): 
         wrong_matrix[i, j] = nb_wrong_tags(t1,t2,wrongpair)
      # convert the matrix to a df for better readability
  tags_wrong_df = pd.DataFrame(wrong_matrix, columns = list(T), index=list(T)) 
  tags_wrong_df.apply(lambda x: x*100/x.sum() , axis=1).round(2).astype(str) + '%'
  dfi.export(tags_wrong_df,"./output/pic1.png") 
  return tags_wrong_df  





def number_each_tag(data_test_tags,tags_wrong_df, T):
  all_tags_df = pd.DataFrame(index=list(T))  
  all_tags_df["% of Occurence in Test Data"]=[total_nb_tag(t, data_test_tags) for t in T]
  all_tags_df["% of missclassifications"]=tags_wrong_df.sum(axis=1)
  dfi.export(all_tags_df,"./output/pic2.png") 
  return all_tags_df  

      

        
def report_classification(real_words, pred_words,T):
    a=classification_report(real_words, pred_words, target_names=T, output_dict=True)
    report_df = pd.DataFrame(a).T
    dfi.export(report_df,"./output/Classif_report.png")
    

    

      

